﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Genre:EntityDescription
    {
        public IEnumerable<Book> Books { get; set; }
    }
}
