﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Theme:EntityDescription
    {
        public IEnumerable<Book> Books { get; set; }
    }
}
