﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    public class Book
    {
        [ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PublicationYear { get; set; }
        public string Rating { get; set; }
        public IEnumerable<Author> Authors { get; set; }

        public IEnumerable<Translator> Translators { get; set; }
        public IEnumerable<CopyrightHolder> CopyrightHolders { get; set; }
        public SubscriptionType SubscriptionType { get; set; }
        public IEnumerable<Genre> Genres { get; set; }
        public IEnumerable<Theme> Themes { get; set; }
        public string BookCover { get; set; }
        public IEnumerable<Review> Reviews { get; set; }
        public IEnumerable<ReaderDiary> ReaderDiaries { get; set; }
    }
}
