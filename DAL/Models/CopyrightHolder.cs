﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class CopyrightHolder:EntityDescription
    {
        public IEnumerable<Book> Books { get; set; }
    }
}
