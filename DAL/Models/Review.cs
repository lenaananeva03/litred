﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    public class Review
    {
        [ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        public User Author { get; set; }
        public string Description { get; set; }

        public int Rating { get; set; }
        public Book Book { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now.Date;
}
}
