﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Translator:People
    {
        public IEnumerable<Book> Books { get; set; }
    }
}
