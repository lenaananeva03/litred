﻿namespace DAL.Models
{
    public class EntityDescription
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}
