﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class Author:People
    {
        public string Biography { get; set; }
        public IEnumerable<Book> Books { get; set; }
    }
}
