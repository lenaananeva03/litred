﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class ReadingStatus : EntityDescription
    {
        public IEnumerable<ReaderDiary> ReaderDiaries { get; set; }
    }
}
