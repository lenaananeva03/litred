﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Interfaces;
using DAL.Models;

namespace DAL.Repositories
{
    public class SubscriptionRepository : IRepository<Subscription>
    {
        private ApplicationContext _db;

        public SubscriptionRepository(ApplicationContext context)
        {
            _db = context;
        }

        public IEnumerable<Subscription> GetAll()
        {
            return _db.Subscriptions;
        }

        public Subscription Get(int id)
        {
            return _db.Subscriptions.Find(id);
        }

        public IEnumerable<Subscription> Find(Func<Subscription, bool> predicate)
        {
            return _db.Subscriptions.Where(predicate).ToList();
        }

        public void Create(Subscription item)
        {
            _db.Subscriptions.Add(item);
        }

        public void Update(Subscription item)
        {
            _db.Subscriptions.Update(item);
        }

        public void Delete(int id)
        {
            Subscription subscription = _db.Subscriptions.Find(id);
            if (subscription != null)
                _db.Subscriptions.Remove(subscription);
        }
    }
}
