﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class BookRepository : IRepository<Book>
    {
        private ApplicationContext _db;

        public BookRepository(ApplicationContext context)
        {
            _db = context;
        }

        public IEnumerable<Book> GetAll()
        {
            return _db.Books;
        }

        public Book Get(int id)
        {
            return _db.Books.Find(id);
        }

        public IEnumerable<Book> Find(Func<Book, bool> predicate)
        {
            return _db.Books.Where(predicate).ToList();
        }

        public void Create(Book item)
        {
            _db.Books.Add(item);
        }

        public void Update(Book item)
        {
            _db.Books.Update(item);
        }

        public void Delete(int id)
        {
            Book book = _db.Books.Find(id);
            if (book != null)
                _db.Books.Remove(book);
        }

        public Book GetBookWithReadingStatus(string bookId, string userId)
        {
            return _db.Books.Include(b => b.Authors)
                .Include(b => b.Translators)
                .Include(b => b.CopyrightHolders)
                .Include(b => b.SubscriptionType)
                .Include(b => b.Genres)
                .Include(b => b.Themes)
                .Include(b => b.Reviews)
                .Include(b => b.ReaderDiaries
                    .Where(rd => rd.Book.Id == bookId && rd.User.Id == userId))
                .ThenInclude(rd => rd.ReadingStatus)
                .Include(b => b.Reviews)
                .ThenInclude(r => r.Author)
                .FirstOrDefault(b => b.Id == bookId);
        }
    }
}
