﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models;
using WebApplication.ViewModels.Admin;

namespace WebApplication.Controllers
{
    [Authorize(Roles = "admin, manager")]
    public class AdminController : Controller
    {
        private readonly ApplicationContext _db;

        public AdminController(ApplicationContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult BooksList()
        {
            var books = _db.Books;
            var model = new BooksListAdminViewModel
            {
                Books = books,
            };
            return View(model);
        }

        [HttpGet]
        public IActionResult BookCreate()
        {
            return View();
        }

        [HttpPost]
        public IActionResult BookCreate(BookCreateAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var book = new Book
                {
                    Title = model.Title,
                    Description = model.Description,
                    PublicationYear = model.PublicationYear,
                };
                _db.Books.Add(book);
                _db.SaveChanges();
                return RedirectToAction("BooksList");
            }

            return View();
        }

        public IActionResult BookEdit(string id)
        {
            var book = _db.Books.Find(id);
            var model = new BookCreateAdminViewModel()
            {
                Id = book.Id,
                Title = book.Title,
                Description = book.Description,
                PublicationYear = book.PublicationYear,
                Rating = book.Rating
            };
            return View(model);
        }
        
        [HttpPost]
        public IActionResult BookEdit(BookCreateAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var book = _db.Books.Find(model.Id);
                Console.WriteLine(book.PublicationYear);
                book.Title = model.Title;
                book.Description = model.Description;
                book.PublicationYear = model.PublicationYear;
                Console.WriteLine(book.PublicationYear);
                _db.SaveChanges();
                return RedirectToAction("BooksList");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult BookDelete(BookCreateAdminViewModel model)
        {
            var book = _db.Books.Find(model.Id);
            if (book is null)
                return NotFound();
            _db.Books.Remove(book);
            _db.SaveChanges();
            return RedirectToAction("BooksList");
        }
    }
}
