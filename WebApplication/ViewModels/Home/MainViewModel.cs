﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models;
using WebApplication.Models;

namespace WebApplication.ViewModels
{
    public class MainViewModel
    {
        public UserViewModel UserModel { get; set; }
        public  IEnumerable<DAL.Models.Book> Books { get; set; }
    }
}
