﻿using System.Collections.Generic;

namespace WebApplication.ViewModels.Admin
{
    public class BooksListAdminViewModel
    {
        public IEnumerable<DAL.Models.Book> Books { get; set; }
    }
}
