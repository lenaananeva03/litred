﻿using System.Collections.Generic;
using DAL.Models;
using Microsoft.AspNetCore.Http;

namespace WebApplication.ViewModels.Admin
{
    public class BookCreateAdminViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PublicationYear { get; set; }
        public string Rating { get; set; }
        // public IEnumerable<Author> Authors { get; set; }
        // public IEnumerable<Translator> Translators { get; set; }
        // public IEnumerable<CopyrightHolder> CopyrightHolders { get; set; }
        // public SubscriptionType SubscriptionType { get; set; }
        // public IEnumerable<Genre> Genres { get; set; }
        // public IEnumerable<Theme> Themes { get; set; }
        // public IFormFile BookCover { get; set; }
        // public string ImagePath { get; set; }
    }
}
