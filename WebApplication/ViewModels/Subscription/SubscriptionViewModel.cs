﻿namespace WebApplication.ViewModels.Subscription
{
    public class SubscriptionViewModel
    {
        public DAL.Models.Subscription Subscription { get; set; }
        public string UserId { get; set; }
    }
}
